FROM php:7.4.16-fpm
RUN apt update; \
    apt upgrade -y;
RUN docker-php-ext-install pdo pdo_mysql mysqli

# CMD [ "chown", "-R", "$USER:www-data", "/var/www/html/tmp" ]