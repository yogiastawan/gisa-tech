<?php
class HomeController extends WebController{
    function index()
    {
        //get post from database
        $posts=new Posts($this->db);
        $this->f3->set('posts',$posts->find());

        $this->f3->set('content','pages/index.html');
    }
}