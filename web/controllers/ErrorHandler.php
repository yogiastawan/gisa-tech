<?php
class ErrorHandler extends WebController{
    function error_handler()
    {
        $code=$this->f3->get('ERROR.code');
        $status=$this->f3->get('ERROR.status');
        if($code===404){
            $this->f3->set('title','Error: '.$code.' - '.$status);
            $this->f3->set('content','pages/error_404_content.html');
        }
        else{
            $this->f3->set('title','Error: '.$code.'-'.$status);
            $this->f3->set('content',$this->f3->get('ERROR.text'));
        }
    }
}