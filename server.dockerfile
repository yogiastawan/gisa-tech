FROM fedora
ENV container docker
RUN sudo dnf install httpd -y
RUN echo 'ServerName localhost' >> /etc/httpd/conf/httpd.conf
RUN dnf install php-common -y
# RUN /usr/sbin/php-fpm -D
ADD ./other/httpd.conf  /etc/httpd/conf/httpd.conf
# ADD www.conf /etc/php-fpm.d/www.conf

ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
# ENTRYPOINT /usr/sbin/php-fpm --nodaemonize
