<?php
require (__DIR__.'/vendor/autoload.php');
$f3= Base::instance();
$f3->config(__DIR__.'/setting/config.ini');
$f3->config(__DIR__.'/setting/routes.ini');

$f3->set('ONERROR','ErrorHandler->error_handler');
$f3->run();